
/*
 *  Autor: Zoé Magalhães (zr.magal@gmail.com)
 *  Mestrando do PPMEC-Unb, matrícula 170172767
 *  Orientador: André Murilo
 *
 *  Leitura dos sensores
 */

#ifndef _header_measurement_
#define _header_measurement_

#include <Eigen/Dense>
#include "settings.hpp" 
using Eigen::MatrixXd;
using Eigen::VectorXd;

#define MEASSTATE_SLIP_ANGLE (0)
#define MEASSTATE_YAW_RATE (1)
#define MEASSTATE_ROLL_RATE (2)
#define MEASSTATE_ROLL_ANGLE (3)

/**
 * Variaveis medidas
 */
class measurements
{
  
    public:
    double steer_angle_rad;
    VectorXd states;

    /**
     * @brief Inicia o acesso aos sensores
     */
    measurements( void );
    
    /**
     * @brief Encerra o acesso aos sensores liberando os recursos 
     */
    ~measurements( void );

    /**
     * @brief Leitura das variaveis medidas
     */
     void update(void); 
};


#endif
