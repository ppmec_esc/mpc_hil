/*
 *  Autor: Zoé Magalhães (zr.magal@gmail.com)
 *  Mestrando do PPMEC-Unb, matrícula 170172767
 *  Disciplina: Controle Preditivo 01/2018
 *  Professor: André Murilo
 *
 *  Controle de estabilidade veicular lateral
 *  pela aplicacao de um controle preditivo
 *  baseado em modelo linear. Aplicacao
 *  desenvolvida para simulacao HIL  
 */

#include <fstream>
#include <sstream>
#include <iostream>
#include <signal.h>
#include <sys/time.h>
#include <arpa/inet.h>
#include <sys/socket.h>
#include <stdlib.h>

#include "mpc.hpp"
#include "measurement.hpp"
#include "settings.hpp"
using namespace std;

#define PORT (65000)
#define BUFLEN (512)
#define SERVER ("10.10.10.6")

static MPC mpc;
static measurements meas;
int s,slen;
struct sockaddr_in si_me,si_other;
bool endrun = false;

void catchkill(int sig_num)
{
	endrun = true;
}

void catch_alarm_setitimer (int sig_num)
{
   static int a = 0;
   double desired_yaw_rate;
   VectorXd sol = VectorXd::Zero(1);
   double comm; 
   desired_yaw_rate = fabs( VEH_SPEED*meas.steer_angle_rad/(VEH_L*(1.0+KU*VEH_SPEED2)) );
   
   if(desired_yaw_rate > MAX_YAW_RATE)
   {
	desired_yaw_rate = MAX_YAW_RATE;
   }

   if( meas.steer_angle_rad < 0.0)
   {
   	desired_yaw_rate = -desired_yaw_rate;
   }


   mpc.get_next_command( &meas,desired_yaw_rate, &sol  ); 
   comm = sol(0);
   sendto(s,&comm,sizeof(comm), 0, (struct sockaddr *)&si_other, slen);
   meas.update();
   //cout << "\r\n comm" << comm << "\r\n";
}

void intFinalize(int sig_num) 
{
    cout << "Done.\n";
    exit(0);
}

int main()
{ 
    s = socket(AF_INET,SOCK_DGRAM,IPPROTO_UDP);

    slen = sizeof(si_other);
    memset((char *)&si_other,0,sizeof(si_other));
    si_other.sin_family = AF_INET;
    si_other.sin_port = htons(PORT);

    inet_aton(SERVER,&si_other.sin_addr);

    signal(SIGINT, catchkill);
    signal(SIGALRM, catch_alarm_setitimer);
    signal(SIGINT, intFinalize);
    
    meas.update();
    ualarm(UPDATE_TIME_US, UPDATE_TIME_US);
    
    while(1){
    	usleep(100);
    }

 //  while(!endrun)
 //  {
//catch_alarm_setitimer(0);
 //  }

   printf("kill");

   printf("\r\n");
   
    return 0;
}
