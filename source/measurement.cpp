/*
 *  Autor: Zoé Magalhães (zr.magal@gmail.com)
 *  Mestrando do PPMEC-Unb, matrícula 170172767
 *  Orientador: André Murilo
 *
 *  Leitura dos sensores
 */
//BIBLIOTECAS NECESSARIAS PARA ANALOG_READ

#include <stdio.h>
#include <fstream>
#include <sstream>
#include <iostream>
#include <stdlib.h> 
#include <math.h>
#include "settings.hpp"
#include "measurement.hpp"
#include "c_wrapper/pruio.h"
#include "c_wrapper/pruio_pins.h"
using namespace std;

#define ADC_LAT_VEL     ( 1 )
#define ADC_YAW_RATE    ( 2 )
#define ADC_ROLL_RATE   ( 4 )
#define ADC_ROLL_ANGLE  ( 5 )
#define ADC_STEER_ANGLE ( 6 )

#define ADC_RAW_TO_ANALOG( _raw_, _out_min_, _out_max_  ) \
((_raw_)*((_out_max_) - (_out_min_))/(65520.0f) + (_out_min_))

pruIo *_io;


const double MAX_MEASURED_VALUE[] = 
{ 
    LAT_VEL_MAX,
    YAW_RATE_MAX,
    ROLL_RATE_MAX,
    ROLL_ANGLE_MAX,
    STEER_ANGLE_MAX 
};


const double  MIN_MEASURED_VALUE[] = 
{ 
    LAT_VEL_MIN,
    YAW_RATE_MIN,
    ROLL_RATE_MIN,
    ROLL_ANGLE_MIN,
    STEER_ANGLE_MIN 
};

measurements::measurements(void)
{
    _io = pruio_new(PRUIO_DEF_ACTIVE, 0x98, 0, 1);
    if( _io->Errr )
    {
        printf("\r\n pruio_new %d", _io->Errr);
         return;
    }

    if(pruio_config(_io,1,0x1FE,0,4))
    {
        printf("\r\n pruio_config error");
         return;
    }
    
    steer_angle_rad = 0.0;
    states.setZero(STATES_SIZE,1); 
}

measurements::~measurements( void )
{
    pruio_destroy(_io);
}

void measurements::update(void)
{
    states(MEASSTATE_SLIP_ANGLE) = 
       atan2( ADC_RAW_TO_ANALOG( (double)_io->Adc->Value[ADC_LAT_VEL],
                                 LAT_VEL_MIN, LAT_VEL_MAX),
				 VEH_SPEED);
    states(MEASSTATE_YAW_RATE)  = 
       ADC_RAW_TO_ANALOG( (double)_io->Adc->Value[ADC_YAW_RATE],
                          YAW_RATE_MIN, YAW_RATE_MAX );
    states(MEASSTATE_ROLL_RATE) = 
       ADC_RAW_TO_ANALOG( (double)_io->Adc->Value[ADC_ROLL_RATE],
                          ROLL_RATE_MIN, ROLL_RATE_MAX );
    states(MEASSTATE_ROLL_ANGLE) =
       ADC_RAW_TO_ANALOG( (double)_io->Adc->Value[ADC_ROLL_ANGLE],
                          ROLL_ANGLE_MIN, ROLL_ANGLE_MAX );
    steer_angle_rad = 
        ADC_RAW_TO_ANALOG( (double)_io->Adc->Value[ADC_STEER_ANGLE],
                          STEER_ANGLE_MIN, STEER_ANGLE_MAX );

    //cout << states << "S" << steer_angle_rad << "\r\n\r\n\r\n" ;			 

}

