CC := arm-linux-gnueabihf-g++
CFLAGS =  -g  -pg -fPIC -DLINUX -D__USE_LONG_INTEGERS__ -D__USE_LONG_FINTS__ -D__NO_COPYRIGHT__


SRCFILES := $(wildcard source/*.cpp)

all: 
	$(CC) $(CFLAGS) source/*.cpp -o bin/main -I./includes/ -I./extern/ -I./extern/qpOASES/ -I./extern/Eigen/ -I./extern/pruio/ -L./extern/qpOASES/ -Wl,-rpath=./extern/qpOASES/ -lqpOASES -L./extern/pruio/c_wrapper/ -lpruio

#obj/%.o: source/%.cpp
#	$(CC) $(CFLAGS) -c $< -o $@ -I./includes -I./extern -I/extern/qpOASES -I/usr/local/include/eigen3/ 


.PHONY: clean
clean:
	rm -rf obj/*
	rm -rf bin/*

run:
		bin/main

